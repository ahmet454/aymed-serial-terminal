﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Enum_Alarm_Pc_Program
{
    public class LogLa
    {
        public LogLa(string LogDosya = "Log")
        {
            LogDosyasi = LogDosya + "_" + DateTime.Now.ToString("mm.ss");
            m_exePath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }
        public string LogDosyasi { get; set; }
        private string m_exePath { get; set; }
        List<string> writes = new List<string>();
        public void WriteLine(string logMessage)
        {
            try
            {
                using (StreamWriter w = File.AppendText(m_exePath + "\\" + LogDosyasi + ".txt"))
                {
                    Log(logMessage, w);
                }
            }
            catch (Exception ex)
            {
            }
        }
        public void AddLog(string LogMessage)
        {
            if (!Closing)
                writes.Add(LogMessage);
        }
        public bool Closing { get; set; }
        private void WriteDizi(List<string> dizi)
        {
            dizi = writes;
            StreamWriter sw = File.AppendText(m_exePath + "\\" + LogDosyasi.Replace(":", ".") + ".txt");
            sw.WriteLine("Başlangıç : " + dizi.Count);
            for (int i = 0; i < dizi.Count; i++)
            {
                sw.WriteLine(dizi[i]);
            }
            sw.Close();
        }
        public void OnAppClosing()
        {
            Closing = true;
            WriteDizi(writes);
        }
        private void Log(string logMessage, TextWriter txtWriter)
        {
            try
            {

                txtWriter.WriteLine("{0}", logMessage);
            }
            catch (Exception ex)
            {
            }
        }

        internal void AddLog(object p)
        {
            //throw new NotImplementedException();
        }
    }
}
