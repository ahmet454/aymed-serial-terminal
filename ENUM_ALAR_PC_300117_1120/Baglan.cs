﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using Usb_Hid;

namespace Operasyonel_Kara_Aracı
{
    public partial class Baglan : Form
    {
        
        
        static string[] ports = SerialPort.GetPortNames();
        public Baglan()
        {
            InitializeComponent();
        }

        public void Baglan_Load(object sender, EventArgs e)
        {
            /* Pc'ye bağı olan portlar ekleniyor */
            foreach (string port in ports)
            {
                comboBox2.Items.Add(port);
            }

        }

        public void button1_Click(object sender, EventArgs e)
        {
            try
            {
                serialPort1.PortName = comboBox2.Text;
                serialPort1.BaudRate = 9600;
                serialPort1.Open();

                if (serialPort1.IsOpen)
                {
                    toolStripStatusLabel1.Text = comboBox2.Text + " Portuna Bağlanıldı";
                    serialPort1.Write("A");
                    
                }
            }
            catch(Exception ex)
            {
                toolStripStatusLabel1.Text = "Porta Bağlanılamadı + "+ex.Message;

                serialPort1.Close();

            }
        }

        public void button2_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                toolStripStatusLabel1.Text = comboBox2.Text + "Portuna Bağlanıldı";
                this.Visible = false;
                Form g = new Form1();
                g.Show();
                serialPort1.Close();

            }
            else
            {
                toolStripStatusLabel1.Text = "Porta Bağlanılamadı";
            }
        }

        public void button3_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

    }
}
