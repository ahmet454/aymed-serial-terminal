﻿namespace Usb_Hid
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dosyaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yeniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.açToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kaydetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.farklıKaydetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.kapatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.görünümToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.döndürToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sıfırlaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayarlarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.genelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.dilSeçimiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.araçlarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bağlanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yardımVeDestekToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hakkımızdaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnKapat = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chcDiscardın = new System.Windows.Forms.CheckBox();
            this.chcDisOut = new System.Windows.Forms.CheckBox();
            this.btnGoster = new System.Windows.Forms.Button();
            this.alinanTemizle = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtSend = new System.Windows.Forms.TextBox();
            this.ReceiveData = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtSendText = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtInterval = new System.Windows.Forms.TextBox();
            this.btnStartTimer = new System.Windows.Forms.Button();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.seriRecieve = new System.IO.Ports.SerialPort(this.components);
            this.txtBaudrate = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            resources.ApplyResources(this.statusStrip1, "statusStrip1");
            this.statusStrip1.Name = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            resources.ApplyResources(this.toolStripStatusLabel1, "toolStripStatusLabel1");
            this.toolStripStatusLabel1.Spring = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dosyaToolStripMenuItem,
            this.görünümToolStripMenuItem,
            this.ayarlarToolStripMenuItem,
            this.araçlarToolStripMenuItem,
            this.yardımVeDestekToolStripMenuItem});
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            // 
            // dosyaToolStripMenuItem
            // 
            this.dosyaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.yeniToolStripMenuItem,
            this.açToolStripMenuItem,
            this.kaydetToolStripMenuItem,
            this.toolStripSeparator1,
            this.farklıKaydetToolStripMenuItem,
            this.toolStripSeparator2,
            this.kapatToolStripMenuItem});
            this.dosyaToolStripMenuItem.Name = "dosyaToolStripMenuItem";
            resources.ApplyResources(this.dosyaToolStripMenuItem, "dosyaToolStripMenuItem");
            // 
            // yeniToolStripMenuItem
            // 
            this.yeniToolStripMenuItem.Image = global::Enum_Alarm_Pc_Program.Properties.Resources.New_document;
            this.yeniToolStripMenuItem.Name = "yeniToolStripMenuItem";
            resources.ApplyResources(this.yeniToolStripMenuItem, "yeniToolStripMenuItem");
            // 
            // açToolStripMenuItem
            // 
            this.açToolStripMenuItem.Image = global::Enum_Alarm_Pc_Program.Properties.Resources.Folder;
            this.açToolStripMenuItem.Name = "açToolStripMenuItem";
            resources.ApplyResources(this.açToolStripMenuItem, "açToolStripMenuItem");
            // 
            // kaydetToolStripMenuItem
            // 
            this.kaydetToolStripMenuItem.Image = global::Enum_Alarm_Pc_Program.Properties.Resources.Save;
            this.kaydetToolStripMenuItem.Name = "kaydetToolStripMenuItem";
            resources.ApplyResources(this.kaydetToolStripMenuItem, "kaydetToolStripMenuItem");
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // farklıKaydetToolStripMenuItem
            // 
            this.farklıKaydetToolStripMenuItem.Image = global::Enum_Alarm_Pc_Program.Properties.Resources.Down;
            this.farklıKaydetToolStripMenuItem.Name = "farklıKaydetToolStripMenuItem";
            resources.ApplyResources(this.farklıKaydetToolStripMenuItem, "farklıKaydetToolStripMenuItem");
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            resources.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
            // 
            // kapatToolStripMenuItem
            // 
            this.kapatToolStripMenuItem.Image = global::Enum_Alarm_Pc_Program.Properties.Resources.Cancel;
            this.kapatToolStripMenuItem.Name = "kapatToolStripMenuItem";
            resources.ApplyResources(this.kapatToolStripMenuItem, "kapatToolStripMenuItem");
            this.kapatToolStripMenuItem.Click += new System.EventHandler(this.kapatToolStripMenuItem_Click);
            // 
            // görünümToolStripMenuItem
            // 
            this.görünümToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.döndürToolStripMenuItem,
            this.sıfırlaToolStripMenuItem});
            this.görünümToolStripMenuItem.Name = "görünümToolStripMenuItem";
            resources.ApplyResources(this.görünümToolStripMenuItem, "görünümToolStripMenuItem");
            // 
            // döndürToolStripMenuItem
            // 
            this.döndürToolStripMenuItem.Image = global::Enum_Alarm_Pc_Program.Properties.Resources.rotate;
            this.döndürToolStripMenuItem.Name = "döndürToolStripMenuItem";
            resources.ApplyResources(this.döndürToolStripMenuItem, "döndürToolStripMenuItem");
            // 
            // sıfırlaToolStripMenuItem
            // 
            this.sıfırlaToolStripMenuItem.Image = global::Enum_Alarm_Pc_Program.Properties.Resources.Refresh;
            this.sıfırlaToolStripMenuItem.Name = "sıfırlaToolStripMenuItem";
            resources.ApplyResources(this.sıfırlaToolStripMenuItem, "sıfırlaToolStripMenuItem");
            // 
            // ayarlarToolStripMenuItem
            // 
            this.ayarlarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.genelToolStripMenuItem,
            this.toolStripSeparator4,
            this.dilSeçimiToolStripMenuItem});
            this.ayarlarToolStripMenuItem.Name = "ayarlarToolStripMenuItem";
            resources.ApplyResources(this.ayarlarToolStripMenuItem, "ayarlarToolStripMenuItem");
            // 
            // genelToolStripMenuItem
            // 
            this.genelToolStripMenuItem.Image = global::Enum_Alarm_Pc_Program.Properties.Resources.Repair;
            this.genelToolStripMenuItem.Name = "genelToolStripMenuItem";
            resources.ApplyResources(this.genelToolStripMenuItem, "genelToolStripMenuItem");
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            resources.ApplyResources(this.toolStripSeparator4, "toolStripSeparator4");
            // 
            // dilSeçimiToolStripMenuItem
            // 
            this.dilSeçimiToolStripMenuItem.Image = global::Enum_Alarm_Pc_Program.Properties.Resources.User_group;
            this.dilSeçimiToolStripMenuItem.Name = "dilSeçimiToolStripMenuItem";
            resources.ApplyResources(this.dilSeçimiToolStripMenuItem, "dilSeçimiToolStripMenuItem");
            // 
            // araçlarToolStripMenuItem
            // 
            this.araçlarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bağlanToolStripMenuItem});
            this.araçlarToolStripMenuItem.Name = "araçlarToolStripMenuItem";
            resources.ApplyResources(this.araçlarToolStripMenuItem, "araçlarToolStripMenuItem");
            // 
            // bağlanToolStripMenuItem
            // 
            this.bağlanToolStripMenuItem.Image = global::Enum_Alarm_Pc_Program.Properties.Resources.plug;
            this.bağlanToolStripMenuItem.Name = "bağlanToolStripMenuItem";
            resources.ApplyResources(this.bağlanToolStripMenuItem, "bağlanToolStripMenuItem");
            this.bağlanToolStripMenuItem.Click += new System.EventHandler(this.bağlanToolStripMenuItem_Click);
            // 
            // yardımVeDestekToolStripMenuItem
            // 
            this.yardımVeDestekToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hakkımızdaToolStripMenuItem,
            this.toolStripSeparator3});
            this.yardımVeDestekToolStripMenuItem.Name = "yardımVeDestekToolStripMenuItem";
            resources.ApplyResources(this.yardımVeDestekToolStripMenuItem, "yardımVeDestekToolStripMenuItem");
            // 
            // hakkımızdaToolStripMenuItem
            // 
            this.hakkımızdaToolStripMenuItem.Image = global::Enum_Alarm_Pc_Program.Properties.Resources.Info;
            this.hakkımızdaToolStripMenuItem.Name = "hakkımızdaToolStripMenuItem";
            resources.ApplyResources(this.hakkımızdaToolStripMenuItem, "hakkımızdaToolStripMenuItem");
            this.hakkımızdaToolStripMenuItem.Click += new System.EventHandler(this.hakkımızdaToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            resources.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            resources.ApplyResources(this.comboBox2, "comboBox2");
            this.comboBox2.Name = "comboBox2";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnStart);
            this.groupBox1.Controls.Add(this.btnKapat);
            this.groupBox1.Controls.Add(this.comboBox2);
            this.groupBox1.Controls.Add(this.label1);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // btnStart
            // 
            this.btnStart.Image = global::Enum_Alarm_Pc_Program.Properties.Resources.connectionplug;
            resources.ApplyResources(this.btnStart, "btnStart");
            this.btnStart.Name = "btnStart";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnKapat
            // 
            this.btnKapat.Image = global::Enum_Alarm_Pc_Program.Properties.Resources.unplugicon;
            resources.ApplyResources(this.btnKapat, "btnKapat");
            this.btnKapat.Name = "btnKapat";
            this.btnKapat.UseVisualStyleBackColor = true;
            this.btnKapat.Click += new System.EventHandler(this.btnKapat_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox3);
            this.groupBox6.Controls.Add(this.groupBox1);
            resources.ApplyResources(this.groupBox6, "groupBox6");
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chcDiscardın);
            this.groupBox3.Controls.Add(this.chcDisOut);
            this.groupBox3.Controls.Add(this.btnGoster);
            this.groupBox3.Controls.Add(this.alinanTemizle);
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // chcDiscardın
            // 
            resources.ApplyResources(this.chcDiscardın, "chcDiscardın");
            this.chcDiscardın.Name = "chcDiscardın";
            this.chcDiscardın.UseVisualStyleBackColor = true;
            // 
            // chcDisOut
            // 
            resources.ApplyResources(this.chcDisOut, "chcDisOut");
            this.chcDisOut.Name = "chcDisOut";
            this.chcDisOut.UseVisualStyleBackColor = true;
            // 
            // btnGoster
            // 
            resources.ApplyResources(this.btnGoster, "btnGoster");
            this.btnGoster.Name = "btnGoster";
            this.btnGoster.UseVisualStyleBackColor = true;
            this.btnGoster.Click += new System.EventHandler(this.btnGoster_Click_1);
            // 
            // alinanTemizle
            // 
            resources.ApplyResources(this.alinanTemizle, "alinanTemizle");
            this.alinanTemizle.Name = "alinanTemizle";
            this.alinanTemizle.UseVisualStyleBackColor = true;
            this.alinanTemizle.Click += new System.EventHandler(this.alinanTemizle_Click_1);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtSend);
            this.groupBox2.Controls.Add(this.ReceiveData);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox5);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // txtSend
            // 
            resources.ApplyResources(this.txtSend, "txtSend");
            this.txtSend.Name = "txtSend";
            this.txtSend.ReadOnly = true;
            // 
            // ReceiveData
            // 
            resources.ApplyResources(this.ReceiveData, "ReceiveData");
            this.ReceiveData.Name = "ReceiveData";
            this.ReceiveData.ReadOnly = true;
            // 
            // groupBox4
            // 
            resources.ApplyResources(this.groupBox4, "groupBox4");
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.TabStop = false;
            // 
            // groupBox5
            // 
            resources.ApplyResources(this.groupBox5, "groupBox5");
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Enum_Alarm_Pc_Program.Properties.Resources.aymedlogo31;
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // txtSendText
            // 
            resources.ApplyResources(this.txtSendText, "txtSendText");
            this.txtSendText.Name = "txtSendText";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label3);
            this.groupBox7.Controls.Add(this.txtBaudrate);
            this.groupBox7.Controls.Add(this.label2);
            this.groupBox7.Controls.Add(this.txtInterval);
            this.groupBox7.Controls.Add(this.btnStartTimer);
            resources.ApplyResources(this.groupBox7, "groupBox7");
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.TabStop = false;
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // txtInterval
            // 
            this.txtInterval.CausesValidation = false;
            resources.ApplyResources(this.txtInterval, "txtInterval");
            this.txtInterval.Name = "txtInterval";
            // 
            // btnStartTimer
            // 
            resources.ApplyResources(this.btnStartTimer, "btnStartTimer");
            this.btnStartTimer.Name = "btnStartTimer";
            this.btnStartTimer.UseVisualStyleBackColor = true;
            this.btnStartTimer.Click += new System.EventHandler(this.btnStartTimer_Click);
            // 
            // seriRecieve
            // 
            this.seriRecieve.BaudRate = 38400;
            this.seriRecieve.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.SeriPort_DataReceived);
            // 
            // txtBaudrate
            // 
            resources.ApplyResources(this.txtBaudrate, "txtBaudrate");
            this.txtBaudrate.Name = "txtBaudrate";
            this.txtBaudrate.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // Form1
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtSendText);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.groupBox7);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosed);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dosyaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yeniToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem açToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kaydetToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem farklıKaydetToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem kapatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem görünümToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayarlarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem araçlarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem genelToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem dilSeçimiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yardımVeDestekToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hakkımızdaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem döndürToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sıfırlaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bağlanToolStripMenuItem;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnKapat;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnGoster;
        private System.Windows.Forms.Button alinanTemizle;
        private System.Windows.Forms.TextBox ReceiveData;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox chcDisOut;
        private System.Windows.Forms.CheckBox chcDiscardın;
        private System.Windows.Forms.TextBox txtSend;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtSendText;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.IO.Ports.SerialPort serialPort1;
        private System.IO.Ports.SerialPort seriRecieve;
        private System.Windows.Forms.Button btnStartTimer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtInterval;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBaudrate;
    }
}

