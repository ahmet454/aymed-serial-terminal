﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Enum_Alarm_Pc_Program
{

    public class DogrulamaStudy
    {
        public List<Dogrulama> Paketler = new List<Dogrulama>();
        public List<Dogrulama> PaketlerGruplanmıs = new List<Dogrulama>();
        public int Interval { get; set; }
        public int Baud { get; set; }
        public DateTime Tarih { get; set; }

        public string UygulamaVersiyon { get; set; }


    }


    public class Dogrulama
    {
        public int PaketSayac { get; set; }
        public int Count { get; set; }
        public double Time { get; set; }
        public List<string> Dizi { get; set; }

        //public bool IsTruePacketFunc()
        //{
        //    bool isTrue = false;

        //    isTrue = Dizi[Dizi.Count - 1] == 255 && Dizi[Dizi.Count - 2] == 0 && Dizi[Dizi.Count - 3] == 0;

        //    return isTrue;
        //}
    }
}
