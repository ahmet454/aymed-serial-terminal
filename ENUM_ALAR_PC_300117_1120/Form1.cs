﻿using Operasyonel_Kara_Aracı;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Diagnostics;
using System.Threading;
using System.IO;
using Enum_Alarm_Pc_Program;

namespace Usb_Hid
{

  

    public partial class Form1 : Form
    {
        static string[] ports = SerialPort.GetPortNames();
        string Gelen = string.Empty;
        public string tarihSaatBuf;
        BackgroundWorker bw = new BackgroundWorker();
        List<Dogrulama> DogrulamaObjesi = new List<Dogrulama>();
        public Form1()
        {
            InitializeComponent();

            bw.DoWork += Bw_DoWork;
            bw.RunWorkerCompleted += Bw_RunWorkerCompleted;
            bw.WorkerSupportsCancellation = true;
            btnStartTimer.Enabled = false;
        }
        int sayac,sayac2=0;
        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            workerSayac = 0;
        }
        bool cancelled = false;
        int workerSayac = 0;
        private void Bw_DoWork(object sender, DoWorkEventArgs e)
        {


            while (!cancelled)
            {
                Thread.Sleep(WorkerInterval);
                if (cancelled) return;

                workerSayac++;
                SendUsbCalibPacket();
            }
            
            
              
            
            
           
        }
        public DogrulamaStudy TestDogrulama { get; set; }
        private void Form1_Load(object sender, EventArgs e)
        {
            TestDogrulama = new DogrulamaStudy();
            
            //textBox1.Text=("Yön tuşlarını kullanınız.(wasd)");
            CheckForIllegalCrossThreadCalls = false;
            this.KeyPreview = true;
            
            /* Pc'ye bağı olan portlar ekleniyor */
            foreach (string port in ports)
            {
                comboBox2.Items.Add(port);
            }

            txtInterval.Text = WorkerInterval.ToString();
            try
            {
                using (StreamReader sr = new StreamReader(@"txt.log"))
                {
                    txtSendText.Text = sr.ReadToEnd();
                }
                using (StreamReader sr2 = new StreamReader(@"txtInterval.log"))
                {
                    txtInterval.Text = sr2.ReadToEnd();
                }
                using (StreamReader sr3 = new StreamReader(@"txtBaud.log"))
                {
                    txtBaudrate.Text = sr3.ReadToEnd();
                    Baudrate = Convert.ToInt32(txtBaudrate.Text);
                }
            }
            catch (Exception)
            {

                txtSendText.Text = byteArrayToString(SendArray);
                txtInterval.Text = WorkerInterval.ToString();
                txtBaudrate.Text = Baudrate.ToString();
            }

        }


        public int Baudrate = 115200;

        private void LedPIC()
        {
            throw new NotImplementedException();
        }


        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                serialPort1.Close();
            }
          
        }

    

        
        private void kapatToolStripMenuItem_Click(object sender, EventArgs e)
        {

          


            System.Windows.Forms.Application.Exit();
        }

        private void Form1_FormClosed(object sender, FormClosingEventArgs e)
        {
            


            System.Windows.Forms.Application.Exit();
        }

        private void bağlanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form g = new Baglan();
            g.Show();
        }
      

        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                serialPort1.PortName = comboBox2.Text;
                Baudrate = Convert.ToInt32(txtBaudrate.Text);
                serialPort1.BaudRate = Baudrate; //38400;
                //serialPort1.ReadBufferSize = 64;
                //serialPort1.WriteTimeout = 200;
                //serialPort1.ReadTimeout = 500;
                serialPort1.Open();

                if (serialPort1.IsOpen)
                {
                    toolStripStatusLabel1.Text = "Cihaza Bağlanıldı";
                    btnStartTimer.Enabled = true;
                    serialPort1.DataReceived += SeriPort_DataReceived;
                }
            }
            catch (Exception ex)
            {
                toolStripStatusLabel1.Text = "Cihaza Bağlanılamadı";
                serialPort1.DataReceived -= SeriPort_DataReceived;
                bool isopen = serialPort1.IsOpen;
                toolStripStatusLabel1.Text += ". serialPort1.IsOpen : " + isopen+" Ex : "+ex.Message;
            }
        }

        private void btnKapat_Click(object sender, EventArgs e)
        {
            serialPort1.Close();
            if (serialPort1.IsOpen)
            {
                toolStripStatusLabel1.Text = "Cihaza Hâlâ Bağlı.";
            }
            else {
                toolStripStatusLabel1.Text = "Cihaz Kapatıldı";
            }
        }

        private void hakkımızdaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form g = new Hakkimizda();
            g.Show();
        }
        Stopwatch sw = new Stopwatch();
        private void SeriPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
               
                //Gelen = serialPort1.ReadExisting();
                byte[] readBuf = new byte[serialPort1.BytesToRead];
                 serialPort1.Read(readBuf, 0, readBuf.Length);
                for (int i = 0; i < readBuf.Length; i++)
                {
                    Gelen += "-" + readBuf[i];
                }
                this.Invoke(new EventHandler(DisplayText));
            }
            catch (Exception hata)
            {
                MessageBox.Show("Hata:" + hata.Message);
            }
        }
        LogLa logs = new LogLa();
        private void DisplayText(object s, EventArgs e)
        {
            //ReceiveData.Text += Math.Round(Convert.ToDouble((sw.ElapsedMilliseconds / 1000))).ToString()+ "-->  "+Gelen+"\r\n";
            //String.Format("{0:0.##}", total1)
            ReceiveData.Text += String.Format("{1:0}-{0:0.0000}", (float)sw.ElapsedMilliseconds / 1000,workerSayac) + " -->  " + Gelen + "\r\n";
            try
            {
                TestDogrulama.Paketler.Add(new Dogrulama() { Dizi = Gelen.Split('-').ToList(), PaketSayac = workerSayac, Time = (float)sw.ElapsedMilliseconds / 1000 });

            }
            catch (Exception ex)
            {
                logs.AddLog("Sayac : "+workerSayac+"  HATA :  "+ ex.Message);

            }
            ReceiveData.SelectionStart = ReceiveData.Text.Length;
            ReceiveData.ScrollToCaret();
            Gelen = String.Empty;
            SerialPort prt = serialPort1;
            //ReceiveData.Text += "\r\n";
        }

        

        

        private void Write(string p)
        {
            throw new NotImplementedException();
        }

        private void alinanTemizle_Click(object sender, EventArgs e)
        {
            ReceiveData.Clear();
        }

        
        private void SendUsbCalibPacket()
        {

            //serialPort1.DiscardOutBuffer();
            //serialPort1.DiscardInBuffer();
            //serialPort1.BaseStream.Flush();
            //serialPort1.Write("SendData");
            sw.Restart();
            SendArray = strToByteArray(txtSendText.Text);
            serialPort1.Write(SendArray, 0, SendArray.Length);

            serialPort1.DiscardOutBuffer();
            serialPort1.DiscardInBuffer();
            
        }

       

        private void alinanTemizle_Click_1(object sender, EventArgs e)
        {
            ReceiveData.Clear();
        }
        int CominicationIndex = 0;
        private void btnGoster_Click_1(object sender, EventArgs e)
        {
          
        
            int a = 1;

            try
            {
              

                //serialPort1.WriteLine(txtSendText.Text);
               
                SendUsbCalibPacket();
                //byte[] ikili = { 1, 3 };
                //serialPort1.Write(ikili, 0, ikili.Length);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                //if (ex.Message.Contains("Veri geçersiz"))
                //{
                //    //serialPort1.WriteLine(txtSendText.Text);
                //    byte[] ikili = { 1, 3 };
                //    serialPort1.Write(ikili, 0, ikili.Length);
                //}
               
            }

        }
        byte[] strToByteArray(string Deger)
        {
            
            string[] arr = Deger.Split(',');
            byte[] bytes = new byte[arr.Length];
            try
            {
                for (int i = 0; i < arr.Length; i++)
                {
                    bytes[i] = Convert.ToByte(arr[i]);
                }
            }
            catch (Exception)
            {
                if (Deger.LastIndexOf(',') == Deger.Length - 1)
                {
                  Deger=  Deger.Remove(Deger.Length - 1);
                 
                }
                if (Deger.IndexOf(',')==0)
                {
                  Deger=  Deger.Remove(0);
                }
                return strToByteArray(Deger);
            }
            return bytes;
        }
        private void button3_Click(object sender, EventArgs e)
        {
            //serialPort1.Write("Resetle");
        }

        private void serialPort1_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            MessageBox.Show("serialPort1_ErrorReceived");
        }
        int WorkerInterval = 3000;
        LogLa logTxtGelen = new LogLa("TxtContent");
        private void btnStartTimer_Click(object sender, EventArgs e)
        {
            if (!bw.IsBusy)
            {
                try
                {
                    WorkerInterval = Convert.ToInt32(txtInterval.Text);
                }
                catch (Exception) { }
                cancelled = false;
                txtInterval.Text = WorkerInterval.ToString();
                TestDogrulama.Baud = Baudrate;
                TestDogrulama.Interval = WorkerInterval;
                bw.RunWorkerAsync();
                btnStartTimer.Text = "Durdur";
                txtSendText.Enabled = false;
                btnKapat.Enabled = false;
                btnStart.Enabled = false;
            }
            else
            {
                cancelled = true;
                bw.CancelAsync();
            


             
                    //foreach (var item in TestDogrulama.Paketler)
                    //{
                    //    try
                    //    {
                    //        item.Dizi.RemoveAll(s => s == "");
                    //    }
                    //    catch (Exception ex )
                    //    {
                    //    logs.AddLog("Sayac Silme : " + item.PaketSayac);
                    //    }
                    //}


                //for (int i = TestDogrulama.Paketler.Min(m => m.PaketSayac); i <= TestDogrulama.Paketler.Max(m=>m.PaketSayac); i++)
                //{

                //    double ItemTime = 0;
                //    try
                //    {
                //         ItemTime = TestDogrulama.Paketler.Where(w => w.PaketSayac == i).Last().Time;
                //    }
                //    catch (Exception ex)
                //    {
                //        if (ex.Message.Contains("içermiyor"))
                //        {
                //            logs.AddLog(i + "için değer yok ");
                //            continue;
                //        }
                        
                //    }


                //   var KompleDizi=  TestDogrulama.Paketler.Where(w => w.PaketSayac == i).Select(s => s.Dizi).ToList();

                //    List<string> DısDizi = new List<string>();
                //    foreach (var IcDizi in KompleDizi)
                //        DısDizi.AddRange(IcDizi);
                //    Dogrulama DogrulamaItemGrouplandirilmis = new Dogrulama();
                //    DogrulamaItemGrouplandirilmis.PaketSayac = i;
                //    DogrulamaItemGrouplandirilmis.Dizi = DısDizi;
                //    DogrulamaItemGrouplandirilmis.Time =ItemTime;
                //    DogrulamaItemGrouplandirilmis.Count = DısDizi.Count;
                //    TestDogrulama.PaketlerGruplanmıs.Add(DogrulamaItemGrouplandirilmis);

                //}
                //var gr = TestDogrulama.PaketlerGruplanmıs;



                //List<int> Countu20OlamayanlarinIndexleri = new List<int>();
                //Countu20OlamayanlarinIndexleri = gr.Where(c => c.Count != 20).Select(s=>s.PaketSayac).ToList();
                // gr = gr.Where(t => t.Count == 20).ToList();
                //List<int> TimeıAvgdenDüsükOlanlar = new List<int>();
                //List<int> TimeıAvgDenYuksekveEsitOlanlar = new List<int>();
                //double min, max, avg;//Times
                //logs.AddLog("Yanlış Counts : " + Countu20OlamayanlarinIndexleri.Count.ToString() + " Doğru counts(20) : " + gr.Count);
                //min = gr.Min(t => t.Time);
                //max= gr.Max(t => t.Time);
                //avg = gr.Average(t => t.Time);

                //TimeıAvgdenDüsükOlanlar = gr.Where(t => t.Time < avg).Select(s => s.PaketSayac).ToList();
                
                //TimeıAvgDenYuksekveEsitOlanlar = gr.Where(t => t.Time >= avg).Select(s => s.PaketSayac).ToList();


                //logs.AddLog("Countu 20 olmayanların indexleri : " + Countu20OlamayanlarinIndexleri.ToStringFromDizi());
                //logs.AddLog("Countu 20 olanların max : " + max);
                //logs.AddLog("Countu 20 olanların min : " + min);
                //logs.AddLog("Countu 20 olanların avg : " + avg);
                //logs.AddLog("TimeıAvgdenDüsükOlanlar " + TimeıAvgdenDüsükOlanlar.ToStringFromDizi());
                //logs.AddLog("TimeıAvgDenYuksekveEsitOlanlar : " + TimeıAvgDenYuksekveEsitOlanlar.ToStringFromDizi());


              
                try
                {
                    for (int i = 0; i < ReceiveData.Lines.Count(); i++)
                    {
                        logTxtGelen.AddLog(ReceiveData.Lines[i]);
                    }
                }
                catch (Exception ex)
                {

                    logTxtGelen.AddLog("receivedata loglarken hata : " + ex.Message);
                }
                logs.OnAppClosing();
                logTxtGelen.OnAppClosing();
                var deg = TestDogrulama;


                btnStartTimer.Text = "Başlat";
                txtSendText.Enabled = true;
                btnKapat.Enabled = true ;
                btnStart.Enabled = true;

               
            }
          
        }
   
        string byteArrayToString(byte[] array) 
        {
            string sonuc = "";
            for (int i = 0; i < array.Length; i++)
            {
                sonuc += array[i].ToString() + ",";
            }
            return sonuc;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            using (StreamWriter sw = new StreamWriter(@"txt.log", false))
            {
                sw.Write(txtSendText.Text);
            }
            using (StreamWriter sw2 = new StreamWriter(@"txtInterval.log", false))
            {
                sw2.Write(txtInterval.Text);
            }
            using (StreamWriter sw3 = new StreamWriter(@"txtBaud.log", false))
            {
                sw3.Write(txtBaudrate.Text);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        byte[] SendArray = {
                  255,
                  255,
                  255,
                  255,
                  255,
                  11,
                  0,
                  255,
                  255,
                  255,
                  255,
                  3,
                  0,
                  4,
                  0,
                  1,
                  0,
                  66
            };
    }
}
